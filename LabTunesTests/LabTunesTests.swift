//
//  LabTunesTests.swift
//  LabTunesTests
//
//  Created by Ricardo Yepez on 11/9/18.
//  Copyright © 2018 Ricardo Yepez. All rights reserved.
//

import XCTest
@testable import LabTunes

class LabTunesTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        let session = Session.sharedInstance
        session.token = nil
    }
    
    func testCorrectLogin () {
        XCTAssertTrue(User.login(userName: "iOSLab", password: "MyPassword"))
    }
    
    func testWrongLogin(){
        XCTAssertFalse(User.login(userName: "Raul", password: ""))
    }
    
    func testSaveSession(){
        let session = Session.sharedInstance
        let _ = User.login(userName: "iOSLab", password: "223")
        XCTAssertNotNil(session.token)
    }
    
    func testWrongSession(){
        let session = Session.sharedInstance
        let _ = User.login(userName: "", password: "223")
        XCTAssertNil(session.token)
    }
    
    func testExpectedToken()  {
        let _ = User.login(userName: "iOSLab", password: "223")
        let session = Session.sharedInstance
        XCTAssertEqual(session.token!, "1234567890","Token Should Match")
        //XCTAssertNotEqual(<#T##expression1: Equatable##Equatable#>, <#T##expression2: Equatable##Equatable#>)
    }
    
    
    func testFetchWithoutToken(){
        XCTAssertThrowsError(try User.fetchSongs())
    }

}
