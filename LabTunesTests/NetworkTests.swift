//
//  NetworkTests.swift
//  LabTunesTests
//
//  Created by Ricardo Yepez on 11/9/18.
//  Copyright © 2018 Ricardo Yepez. All rights reserved.
//

import XCTest

@testable import LabTunes

class NetworkTests: XCTestCase {
    
    var sessionUnderTest: URLSession!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        sessionUnderTest = URLSession(configuration: .default)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    
    
    func testValidCallToItunes(){
        let url = URL(string: "https://itunes.apple.com/search?media=music&entity=song&term=Queen")
        var statusCode: Int?
        var responseError: Error?
        let promise = expectation(description: "Handler Invoked")
        
        let dataTask = sessionUnderTest.dataTask(with: url!){ data,response,error in
            statusCode = (response as? HTTPURLResponse)?.statusCode
            responseError = error
            promise.fulfill()
        }
        
        dataTask.resume()
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
        
    }
    
    func testSlowValidCallToItunes(){
        let url = URL(string: "https://itunes.apple.com/search?media=music&entity=song&term=abba")
        let promise = expectation(description: "Status code: 200")
        
        let dataTask = sessionUnderTest.dataTask(with: url!){ data,response,error in
            if  let error = error {
                XCTFail("Error: (\(error.localizedDescription))")
            }else if let statusCode = (response as? HTTPURLResponse)?.statusCode{
                if statusCode == 200 {
                    promise.fulfill()
                }else{
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }
        dataTask.resume()
        waitForExpectations(timeout: 5, handler: nil)
        
    }
    
    func testfetchSong() {
        var resultSongs:[Song] = []
        let promise = expectation(description: "Songs Downloaded")
        Music.fetchSong(songName: "Arabella") { (songs) in
            resultSongs = songs
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotEqual(resultSongs.count, 0)
    }
    
}
